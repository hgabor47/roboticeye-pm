// PCA9685-Arduino Servo Evaluator Example
// In this example, we utilize the ServoEvaluator class to assist with setting PWM
// frequencies when working with servos. We will be using Wire1, which is only available
// on boards with SDA1/SCL1 (Due, Zero, etc.) - change to Wire if Wire1 is unavailable.

#include <Wire.h>
#include "PCA9685.h"
#include "math.h"
#define BAL 15
#define JOBB 14
#define FEL 13

#define Znulloffset -25
#define LimitZmax 10 //degree
#define LimitZmin -30

#define Lnulloffset 37
#define LimitLmax 30  //degree
#define LimitLmin -30

#define Rnulloffset -6
#define LimitRmax 30 
#define LimitRmin -30

#define DEG 0.01745329251994


PCA9685 pwmController(Wire, PCA9685_PhaseBalancer_Weaved); // Library using Wire1 and weaved phase balancing scheme

// Linearly interpolates between standard 2.5%/12.5% phase length (102/512) for -90°/+90°
PCA9685_ServoEvaluator pwmServo1;

// Testing our second servo has found that -90° sits at 128, 0° at 324, and +90° at 526.
// Since 324 isn't precisely in the middle, a cubic spline will be used to smoothly
// interpolate PWM values, which will account for said discrepancy. Additionally, since
// 324 is closer to 128 than 526, there is less resolution in the -90° to 0° range, and
// more in the 0° to +90° range.
PCA9685_ServoEvaluator pwmServo2(128,324,526);

void setup() {
    Serial.begin(115200);

    Wire.begin();                      // Wire must be started first
    Wire.setClock(400000);             // Supported baud rates are 100kHz, 400kHz, and 1000kHz

    pwmController.resetDevices();       // Software resets all PCA9685 devices on Wire line

    pwmController.init(B000000);        // Address pins A5-A0 set to B000000
    pwmController.setPWMFrequency(50);  // 50Hz provides 20ms standard servo phase length

  bal(0);  //0=0, 10=7\.342, 20=16\.642,30=25\.866,40=30\.984 
  jobb(0);
  fel(0);

  //nez(80,500,0);
}

double ox=0;
double oy=250;
double z=0;
double r=100;
int i=0; //0..359
int mode=0;

void loop() {
    //return;
    if (++i>359) {
      i=0;
      ++mode;
    }
    //mode=8;
    switch (mode){
      case 0: vonal();break;
      case 1: kor();break;
      case 2: nez(100,100,100);i=360;delay(800);break;
      case 3: nez(-100,300,30);i=360;delay(300);break;
      case 4: nez(-300,900,-20);i=360;delay(1200);break;
      case 5: nez(-100,300,30);i=360;delay(300);break;
      case 6: nez(0,200,-30);i=360;delay(600);break;
      case 7: nez(0,100,-30);i=360;delay(600);break;
      case 8: vonal();break;
      case 9: kor3();break;
      case 10: kor2();break;
      case 11: vonal2();break;
      case 12: vonal2();break;
      default: mode=0;break;
    }
}
void vonal(){
    delay(3);
    nez(ox,40+(i),z-35);
}

void vonal2(){
  delay(10);
  int ii=i % 90;
  if (i<90){
    nez(-45,ii+100,-50);
  }else
  if (i<180){
    nez(-45+ii,190,-50);    
  }else
  if (i<270){
    nez(45,190-ii,-50);    
  }else
  {
    nez(45-ii,100,-50);  
  }
  
}

void kor(){
    double x;
    double y;
    x = ox+sin(i*DEG)*r;
    y = oy+cos(i*DEG)*r;
    Serial.print("area ");Serial.print(x);Serial.print(",");Serial.println(y);
    delay(1);
    nez(x,y,z);
}
void kor2(){
    double x;
    double y;
    oy=200;
    r=60;
    x = ox+sin(i*DEG)*r;
    y = oy+cos(i*DEG)*r;
    Serial.print("area ");Serial.print(x);Serial.print(",");Serial.println(y);
    delay(0);
    nez(x,y,-50);
}
void kor3(){
    double x;
    double y;
    x = ox+sin(i*DEG)*r;
    y = oy+cos(i*DEG)*r;
    Serial.print("area ");Serial.print(x);Serial.print(",");Serial.println(y);
    delay(20);
    nez(x,y,y-40);
}
void nez(double x,double y, double z) { //% -20 ... 20 
   if (y<30) y=30;
   double a1 = atan((35+x)/y);
   double a2 = atan((-35+x)/y);
   double up = atan(z/y);
   Serial.print("NEZ ");Serial.println(up);
   bal(a1/DEG);
   jobb(a2/DEG);
   fel(up/DEG);
}



void bal(double ang){
    if (ang>LimitLmax) ang=LimitLmax;
    if (ang<LimitLmin) ang=LimitLmin;
    ang = asin(ang/50)/(PI/180);
    //Serial.print("L ");Serial.println(ang);
    setpos(BAL,Lnulloffset-ang);    
}
void jobb(double ang){
    if (ang>LimitRmax) ang=LimitRmax;
    if (ang<LimitRmin) ang=LimitRmin;
    ang = asin(ang/62)/(PI/180);
    //Serial.print("R ");Serial.println(ang);
    setpos(JOBB,Rnulloffset-ang);    
}
void fel(double ang){
    if (ang>LimitZmax) ang=LimitZmax;
    if (ang<LimitZmin) ang=LimitZmin;
    setpos(FEL,-ang+Znulloffset);    
}

void setpos(int ch,double ang){
    pwmController.setChannelPWM(ch, pwmServo1.pwmForAngle(ang));
    //Serial.println(pwmController.getChannelPWM(7)); // Should output 102 for -90°
    // Showing linearity for midpoint, 205 away from both -90° and 90°
    //Serial.println(pwmServo1.pwmForAngle(7));   // Should output 307 for 0°
    
}
